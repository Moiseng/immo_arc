from flask import request, Response
from flask import Blueprint

user = Blueprint("user", __name__)

from src.user.service.user_service import UserService


@user.route('/user', methods=['POST'])
def create_user() -> Response:
    body = request.json
    return UserService.create_user(body)


@user.route('/user/<int:user_id>', methods=['PUT'])
def update_user(user_id: int) -> Response:
    body = request.json
    return UserService.update_user(user_id, body)
