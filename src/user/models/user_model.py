from app import db as db


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String, nullable=False)
    lastname = db.Column(db.String, nullable=False)
    birth_date = db.Column(db.String, nullable=False)
    properties = db.relationship('Property', backref='user', lazy=True, cascade='all, delete')

    def __repr__(self):
        return '<User %r>' % self.email

    @property
    def serialized(self) -> dict:
        return {
            "id": self.id,
            "firstname": self.firstname,
            "lastname": self.lastname,
            "birth_date": self.birth_date
        }
