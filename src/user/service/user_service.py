from flask import Response, jsonify
from src.user.repository.user_repository import UserRepository
from src.utils import check_missing_field, check_for_empty_field


class UserService:

    @staticmethod
    def create_user(data: dict) -> Response:
        """
        Create new User
        :param data:
        :return:
        """
        required_fields = ["firstname", "lastname", "birth_date"]
        if len(data) > 0:
            missing_fields = check_missing_field(required_fields, data)
            if len(missing_fields) > 0:
                return jsonify({"error": f"Missing field(s): {missing_fields}"})

            empty_fields = check_for_empty_field(data)

            if len(empty_fields) > 0:
                return jsonify({"error": f"{empty_fields} can't be empty"})

            return jsonify({
                "data": {
                    "user": UserRepository.create_user(data),
                    "status_code": 200
                }
            })
        return jsonify({'error': f'You need to provide valid fields: {required_fields}'})

    @staticmethod
    def update_user(user_id: int, data: dict) -> Response:
        """
        Update user data
        :param user_id: ID of the user to update
        :param data:
        :return:
        """
        user = UserRepository.find_user_by_id(user_id)

        if user is None:
            return jsonify({"error": "The given user ID doesn't match to a record in the database"})

        return jsonify({
            "data": {
                "user": UserRepository.update_user(user, data),
                "status_code": 200
            }
        })

