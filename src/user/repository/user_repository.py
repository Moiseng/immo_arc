from app import db
from src.user.models.user_model import User


class UserRepository:

    @staticmethod
    def create_user(data: dict) -> dict:
        user = User(firstname=data['firstname'], lastname=data['lastname'], birth_date=data['birth_date'])
        db.session.add(user)
        db.session.commit()
        return user.serialized

    @staticmethod
    def update_user(user: User, data: dict) -> dict:
        if data['firstname'] and data['firstname'].strip():
            user.firstname = data['firstname']

        if data['lastname'] and data['lastname'].strip():
            user.lastname = data['lastname']

        if data['birth_date'] and data['birth_date'].strip():
            user.birth_date = data['birth_date']

        db.session.commit()
        return user.serialized

    @staticmethod
    def find_user_by_id(user_id: int) -> User:
        return User.query.filter_by(id=user_id).first()
