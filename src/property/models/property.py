from app import db as db
from src.user.models.user_model import User


class Property(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=True)
    city = db.Column(db.String, nullable=False)
    room_number = db.Column(db.Integer, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    property_type_id = db.Column(db.Integer, db.ForeignKey('property_type.id'), nullable=False)
    property_rooms = db.relationship('PropertyRoom', backref='property', lazy=True, cascade="all, delete")

    @property
    def serialized(self) -> dict:
        """
        Return Object data in Serialized format
        :return:
        """
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'city': self.city,
            'room_number': self.room_number,
            'user_id': self.user_id,
            'property_type_id': self.property_type_id
        }
