from app import db as db


class PropertyType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type_name = db.Column(db.String, nullable=True, unique=True)
    rooms = db.relationship('Property', backref='property_type', lazy=True, uselist=False)
