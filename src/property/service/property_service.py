from __future__ import annotations
from flask import jsonify, Response

from src.property.repository.property_repository import PropertyRepository
from src.utils import check_missing_field


class PropertyService:

    @staticmethod
    def create_property(data: dict, user_id: int) -> Response:
        """
        Create new Property from the given params data

        :param data: dictionary that contains provided key -> value data
        :param user_id: ID of the User ( property owner )
        :return:
        """
        required_fields = ['name', 'description', 'city', 'property_type']
        if len(data) > 0:
            missing_field = check_missing_field(required_fields, data)
            if len(missing_field) > 0:
                return jsonify({'error': f'Missing field(s): {missing_field}'})

            property_type = PropertyRepository.get_property_type_id_by_name(data['property_type'])

            if property_type is None:
                return jsonify({'error': "Provide valid property type : [maison, appartement]"})

            property_type_id = property_type.id

            return jsonify(
                {'data':
                    {
                        'property': PropertyRepository.create_property(data, user_id, property_type_id),
                        'status_code': 200
                    }
                })
        return jsonify({'error': f'You need to provide valid fields: {required_fields}'})

    @staticmethod
    def get_properties_from_city(city: str) -> Response:
        """
        Retrieves all Properties that have same city name
        :param city: given city
        :return: List of properties
        """
        properties = PropertyRepository.get_properties_from_city(city.lower())
        return jsonify(
            {'data':
                 {
                    'properties': [result.serialized for result in properties],
                    'count': len(properties),
                    'status_code': 200
                 }
            })

    @staticmethod
    def update_property(user_id: int, id_property: int, data: dict) -> Response:
        """
        Update a property by property id

        :param user_id: User ID of the property owner
        :param id_property: ID of property to update
        :param data: dictionary that contains our new data
        :return:
        """
        property = PropertyRepository.get_property_by_id(id_property)
        if property is None:
            return jsonify({'error': "Given property id doesn't exist in the database"})

        if property.user_id is not user_id:
            return jsonify({'error': "This property doesn't belong to the given user"})

        return jsonify({
            "data": {
                "property": PropertyRepository.update_property_by_id(property, data),
                "status_code": 200
            }
        })
