from app import db
from typing import List
from src.property.models.property import Property
from src.property.models.property_type import PropertyType


class PropertyRepository:

    @staticmethod
    def create_property(data: dict, user_id: int, property_type_id: int) -> dict:
        property = Property(
            name=data['name'],
            description=data['description'],
            city=data['city'].lower(),
            user_id=user_id,
            property_type_id=property_type_id
        )
        db.session.add(property)
        db.session.commit()

        return property.serialized

    @staticmethod
    def get_properties_from_city(city: str) -> List[Property]:
        return Property.query.filter_by(city=city).all()

    @staticmethod
    def get_property_type_id_by_name(name: str) -> PropertyType:
        return PropertyType.query.filter_by(type_name=name.lower()).first()

    @staticmethod
    def update_property_by_id(property: Property, data: dict) -> dict:
        if data['name'] and data['name'].strip():
            property.name = data['name']

        if data['description'] and data['description'].strip():
            property.description = data['description']

        if data['city'] and data['city'].strip():
            property.city = data['city']

        if data['property_type'] and data['property_type'].strip():
            property_type: PropertyType = PropertyRepository.get_property_type_id_by_name(data['property_type'])
            if property_type is not None:
                property.property_type_id = property_type.id

        db.session.commit()
        return property.serialized

    @staticmethod
    def get_property_by_id(id_property: int) -> Property:
        return Property.query.filter_by(id=id_property).first()

    @staticmethod
    def update_property_room_number(property_id: int, room_number: int) -> Property:
        """
        Update property room number
        :param property_id:
        :param room_number:
        :return:
        """
        property = PropertyRepository.get_property_by_id(property_id)
        property.room_number = room_number
        db.session.commit()
        return property

