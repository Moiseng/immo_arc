from flask import Blueprint, request, Response

property = Blueprint('property', __name__)

from src.property.service.property_service import PropertyService


@property.route('/property/<int:user_id>', methods=['POST'])
def create(user_id: int) -> Response:
    assert user_id > 0, "You can't give a negative user id"
    body = request.json
    return PropertyService.create_property(body, user_id)


@property.route('/property', methods=['GET'])
def get_city_properties() -> Response:
    city = request.args.get('city')
    return PropertyService.get_properties_from_city(city)


@property.route('/property/<int:user_id>/<int:property_id>', methods=['PUT'])
def update_property(user_id: int, property_id: int) -> Response:
    body = request.json
    return PropertyService.update_property(user_id, property_id, body)
