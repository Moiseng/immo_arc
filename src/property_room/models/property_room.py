from app import db as db


class PropertyRoom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    room_specification = db.Column(db.String, nullable=False)
    property_id = db.Column(db.Integer, db.ForeignKey('property.id'), nullable=False)

    @property
    def serialized(self) -> dict:
        """
        Return Object data in Serialized format
        :return:
        """
        return {
            "id": self.id,
            "room_specification": self.room_specification,
            "property_id": self.property_id
        }
