from flask import Blueprint, request, Response

room = Blueprint('room', __name__)

from src.property_room.service.property_room_service import PropertyRoomService


@room.route('/property/room/<int:user_id>/<int:property_id>', methods=["POST"])
def add_room_to_property(property_id: int, user_id: int) -> Response:
    body = request.json
    return PropertyRoomService.add_room_to_property(property_id, user_id, body)


@room.route("/property/room/<int:user_id>/<int:room_id>/<int:property_id>/", methods=["PUT"])
def update_room(user_id: int, room_id: int, property_id: int) -> Response:
    body = request.json
    return PropertyRoomService.update_room(room_id, property_id, body, user_id)


@room.route("/property/room/<int:room_id>", methods=["DELETE"])
def delete_room(room_id: int) -> Response:
    return PropertyRoomService.delete_room(room_id)
