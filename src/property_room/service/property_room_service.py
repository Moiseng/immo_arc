from flask import Response, jsonify
from src.property_room.repository.property_room_repository import PropertyRoomRepository


class PropertyRoomService:

    @staticmethod
    def add_room_to_property(property_id: int, user_id: int, data: dict) -> Response:
        """
        Create new room for a specific property
        :param property_id: property to add room
        :param user_id: user ID of the property owner
        :param data:
        :return:
        """
        from src.property.repository.property_repository import PropertyRepository

        property = PropertyRepository.get_property_by_id(property_id)

        if property is None:
            return jsonify({"error": "Given property id doesn't match to a record in the database"})

        if property.user_id is not user_id:
            return jsonify({"error": "Given user id doesn't match to the property owner id"})

        return jsonify({
            "data": {
                "property": property.name,
                "room": PropertyRoomRepository.add_room_to_property(property.id, data)
            }
        })

    @staticmethod
    def update_room(room_id: int, property_id: int, data: dict, user_id: int) -> Response:
        """
        Update a room

        :param room_id: Room ID
        :param property_id: property ID of the Room to update
        :param data: new data to update
        :param user_id: owner of the room property
        :return:
        """
        from src.property.repository.property_repository import PropertyRepository

        room = PropertyRoomRepository.get_room_by_id(room_id)
        if room is None:
            return jsonify({"error": "Given room id doesn't match to a record in the database"})

        if room.property_id is not property_id:
            return jsonify({"error": "Given property id doesn't match to the room property"})

        property_user = PropertyRepository.get_property_by_id(room.property_id)

        if property_user.user_id is not user_id:
            return jsonify({"error": "This property doesn't belong to the given user"})

        return jsonify({
            "data": {
                "room": PropertyRoomRepository.update_room_of_a_property(room, data),
                "status_code": 200
            }
        })

    @staticmethod
    def delete_room(room_id: int) -> Response:
        """
        Delete a room by ID
        :param room_id:
        :return:
        """
        room = PropertyRoomRepository.get_room_by_id(room_id)
        if room is None:
            return jsonify({"error": "Given room ID doesn't match to a record in the database"})

        PropertyRoomRepository.delete_room(room)
        return jsonify({
            "data": {
                "message": "room deleted successfully",
                "status_data": 200
            }
        })
