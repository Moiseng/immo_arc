from app import db
from src.property.repository.property_repository import PropertyRepository
from src.property_room.models.property_room import PropertyRoom


class PropertyRoomRepository:

    @staticmethod
    def add_room_to_property(property_id: int, data: dict):
        """
        Add new room for given property
        :param property_id: property ID
        :param data: room
        :return:
        """
        room = PropertyRoom(room_specification=data["room_specification"], property_id=property_id)
        db.session.add(room)
        db.session.commit()

        rooms = PropertyRoomRepository.get_property_rooms(property_id)
        PropertyRepository.update_property_room_number(property_id, len(rooms))
        return room.serialized

    @staticmethod
    def update_room_of_a_property(room: PropertyRoom, data: dict) -> dict:
        room.room_specification = data['room_specification']
        db.session.commit()
        return room.serialized

    @staticmethod
    def get_room_by_id(room_id: int) -> PropertyRoom:
        return PropertyRoom.query.filter_by(id=room_id).first()

    @staticmethod
    def delete_room(room: PropertyRoom):
        db.session.delete(room)
        db.session.commit()

    @staticmethod
    def get_property_rooms(property_id: int) -> PropertyRoom:
        rooms: PropertyRoom = PropertyRoom.query.filter_by(property_id=property_id).all()
        return rooms
