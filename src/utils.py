

def check_missing_field(required_fields: list, given_fields: dict) -> list:
    """
    Verify if given fields match with given required fields

    :param required_fields: The required fields
    :param given_fields: Expected fields from body
    :return: list of missing field
    """
    missing_fields = []
    for field in required_fields:
        if field not in given_fields:
            missing_fields.append(field)
    return missing_fields


def check_for_empty_field(given_fields: dict) -> list:
    """
    Verify if given field(s) are empty
    :param given_fields: given field(s) dictionary
    :return:
    """
    empty_fields: list = []
    for key in given_fields:
        if not given_fields[key]:
            empty_fields.append(key)
    return empty_fields
