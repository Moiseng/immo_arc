<<<<<<< README.md
## IMMO ARC API

### Start project

1. Generate virtualenv 
2. Execute next commands

## To install project dependencies
1. In the project directory`pip install requirements.txt`

## To migrate database table ( PostgreSQL DB )
1. In the project folder CLI `flask db init` this command will init a migration directory if it doesn't exist
2. After initializing, execute `flask db migrate` this will create migrations of created tables
3. And to push migration tables to the database `flask db upgrade`

## To run the project
1. `flask run` in the project folder CLI
2. It will give you an url like `http://127.0.0.1:5000`, copy and paste it to your API test tools ( like Postman)

## ROUTES
### ======= USER =======

#### POST /user
#### create new user

`body `

```json
{
  "firstname",
  "lastname",
  "birth_date"
}
```

##### Results
```json
{
    "data": {
        "status_code",
        "user": {
            "birth_date":,
            "firstname":,
            "id":,
            "lastname":
        }
    }
}
```
#### PUT /user/\<user_id>
#### update user

`body `

```json
{
  "firstname",
  "lastname",
  "birth_date"
}
```

##### Results
```json
{
    "data": {
        "status_code",
        "user": {
            "birth_date":,
            "firstname":,
            "id":,
            "lastname":
        }
    }
}
```

### ======= PROPERTY =======

#### POST /property/\<user_id>
#### create new property

`body `

```json
{
    "name":,
    "description":,
    "city":,
    "property_type":
}
```

##### Results
```json
{
    "data": {
        "property": {
            "city":,
            "description":,
            "id":,
            "name":,
            "property_type_id":,
            "room_number":,
            "user_id":
        },
        "status_code":
    }
}
```

#### PUT /property/\<user_id>/\<property_id>
#### update property

`body `

```json
{
    "name":,
    "description":,
    "city":,
    "property_type":
}
```

##### Results
```json
{
    "data": {
        "property": {
            "city":,
            "description":,
            "id":,
            "name":,
            "property_type_id":,
            "room_number":,
            "user_id":
        },
        "status_code":
    }
}
```

#### GET /property
#### Get properties of a city

#### params

* `city`: The properties city

##### Results
```json
{
    "data": {
        "count":,
        "properties": [
            {
                "city":,
                "description":,
                "id":,
                "name":,
                "property_type_id":,
                "room_number":,
                "user_id":
            }
        ],
        "status_code":
    }
}
```

### ======= PROPERTY ROOM =======

#### POST /property/room/\<user_id>/\<property_id>
#### add room to a property

`body `

```json
{
    "room_specification": ""
}
```

##### Results
```json
{
    "data": {
        "property":,
        "room": {
            "id":,
            "property_id":,
            "room_specification":
        }
    }
}
```

#### PUT /property/\<user_id>/\<room_id>/\<property_id>
#### update room

`body `

```json
{
  "room_specification": ""
}
```

##### Results
```json
{
    "data": {
        "room": {
            "id":,
            "property_id":,
            "room_specification":
        },
        "status_code":
    }
}
```

#### DELETE /property/\<room_id>
#### Delete room

##### Results
```json
{
    "data": {
        "message":,
        "status_data":
    }
}
```
