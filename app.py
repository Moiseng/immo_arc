from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


app = Flask(__name__)

# DB connection
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:admin@localhost:5433/immo_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from src.user.routes import user
from src.property.routes import property
from src.property_room.routes import room

app.register_blueprint(user)
app.register_blueprint(property)
app.register_blueprint(room)

if __name__ == "__main__":
    app.run(
        host="localhost",
        debug=True
    )


